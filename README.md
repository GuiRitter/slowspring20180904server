Project created to try to find out why select queries are so slow on Spring with Hibernate and PostgreSQL. Follow the instructions to reproduce.

1. Have a working PostgreSQL installation, version 9.4 or higher.
2. Have a working Maven.
3. Have a working Tomcat.
4. Run the scripts in `src/main/resources/scripts` to initialize the database.
    1. `create_database.sql`
    2. `create_table.sql`
    3. `populate.sql`
    4. Optionally, `create_index.sql` to create an index and run the query faster. How much faster might be an indicator whether the issue comes from PostgreSQL or Spring.
    5. Optionally, `drop_index.sql`, `drop_table.sql` and/or `drop_database`.
5. Edit `src/main/resources/application.properties` to adjust Tomcat's port and PostgreSQL's connection details as needed.
6. Run `mvn clean package` inside the project's folder to generate a `target/slow_spring_2018_09_04.war`.
7. Deploy that `war` on Tomcat on context path `/slow_spring_2018_09_04`.
8. Follow the instructions on the client project.
