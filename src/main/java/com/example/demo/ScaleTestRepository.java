package com.example.demo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "scale_test", path = "scale_tests")
public interface ScaleTestRepository extends CrudRepository<ScaleTest, Integer>, ScaleTestRepositoryCustom {}
