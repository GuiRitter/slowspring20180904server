package com.example.demo;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="scaleTests")
public class ScaleTest implements Serializable {

	private static final long serialVersionUID = 7836275957301561383L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "scale_test_id", columnDefinition="NUMERIC")
	private Integer id;

	@Column(name = "where_test", precision = 17, scale = 5)
	private BigDecimal whereTest;

	@Column(name = "junk", length = 1000, columnDefinition="bpchar")
	private String junk = "junk";

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getJunk() {
		return junk;
	}

	public void setJunk(String junk) {
		this.junk = junk;
	}

	public BigDecimal getWhereTest() {
		return whereTest;
	}

	public void setWhereTest(BigDecimal whereTest) {
		this.whereTest = whereTest;
	}

	public ScaleTest() {}

	public ScaleTest(ScaleTest scaleTest) {
		id = scaleTest.id;
		whereTest = scaleTest.whereTest;
		junk = scaleTest.junk;
	}

	public ScaleTest(BigDecimal whereTest, String junk) {
		this.whereTest = whereTest;
		this.junk = junk;
	}

	@Override
	public String toString() {
		return String.format("{id: %s, whereTest: %s}", id, whereTest);
	}
}
