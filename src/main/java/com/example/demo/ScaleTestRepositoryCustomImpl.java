package com.example.demo;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

@Transactional
public class ScaleTestRepositoryCustomImpl implements ScaleTestRepositoryCustom {

	@Autowired
	EntityManager entityManager;

	@Override
	@SuppressWarnings("unchecked")
	public List<ScaleTest> findScaleTests() {
		Query query;
		long time0, time1;
		time0 = System.nanoTime();
		query = entityManager.createQuery("select scaleTest from ScaleTest as scaleTest where where_test > 0.8");
		time1 = System.nanoTime();
		long time = time1 - time0;
		System.out.format("time to create query: %s ns = %s μs = %s ms = %s s\n", time, time / 1000d, time / 1000000d, time / 1000000000d);
		return query.getResultList();
	}
}
