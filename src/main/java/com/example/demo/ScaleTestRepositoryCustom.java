package com.example.demo;

import java.util.List;

public interface ScaleTestRepositoryCustom {

	List<ScaleTest> findScaleTests();
}
