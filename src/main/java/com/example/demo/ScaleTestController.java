package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RepositoryRestController()
@RequestMapping("scaleTest")
public class ScaleTestController {

	@Autowired
	private ScaleTestRepository repository;

	@GetMapping("fromRepository")
	public ResponseEntity<Object> fromRepository(){

		long time0, time1;
		time0 = System.nanoTime();
		repository.findScaleTests();
		time1 = System.nanoTime();
		return ResponseEntity.ok(time1 - time0);
	}
}
