CREATE TABLE scale_tests (
    scale_test_id integer NOT NULL,
    junk bpchar(1000),
    where_test numeric(17,5)
);

ALTER TABLE scale_tests OWNER TO postgres;

CREATE SEQUENCE scale_tests_scale_test_id_seq
    -- AS integer -- probably only for PostgreSQL 10
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE scale_tests_scale_test_id_seq OWNER TO postgres;

ALTER SEQUENCE scale_tests_scale_test_id_seq OWNED BY scale_tests.scale_test_id;

ALTER TABLE ONLY scale_tests ALTER COLUMN scale_test_id SET DEFAULT nextval('scale_tests_scale_test_id_seq'::regclass);

SELECT pg_catalog.setval('scale_tests_scale_test_id_seq', 1, false);

ALTER TABLE ONLY scale_tests
    ADD CONSTRAINT scale_tests_pkey PRIMARY KEY (scale_test_id);
