DO $do$ DECLARE
BEGIN
    <<i>>
    FOR i IN 1..100000 LOOP
    	INSERT INTO scale_tests (junk, where_test)
			VALUES ('junk', random());
        IF i % 1000 = 1 THEN
        	raise notice '%', i;
        END IF;
    END LOOP;
END $do$;
